# To test locally 

- Clone the projet
- Use gitlab docker image: https://gitlab.com/gitlab-ci-utils/docker-hugo
- Run command :
```bash
docker container run --rm -it -p 1313:1313 -v ${PWD}:/site registry.gitlab.com/gitlab-ci-utils/docker-hugo:latest hugo server --bind 0.0.0.0 --renderToDisk
```
- Open website: http://localhost:1313/website/