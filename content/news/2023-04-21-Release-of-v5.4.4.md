---
title: "Release of v5.4.4"
date: 2023-06-29
---

COMPASS v5.4.4 release notes:

- Debug KL2V when DM has unseen actuators

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)
