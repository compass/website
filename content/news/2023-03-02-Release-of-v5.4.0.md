---
title: "Release of v5.4.0"
date: 2023-03-02
---

COMPASS v5.4.0 release notes:

- Add new GPU-based coronagraph module providing implementation of perfect coronagraph and APLC-like
- Add new parameters *p_wfs.kernconv4imat* to enable/disable the feature of using a kernel convolution on the SH spots during interaction matrix computation. Default value is True to keep the same behaviour as previous versions of COMPASS
- Add new TwoStagesManager class in shesha supervisors: this class shows an example of how to handle 2 stages AO systems with COMPASS.
- Add cupy interface for carmaWrap.CarmaObj object
- Add support for CUDA 12
- Add pySide2 support to have another library available to handle GUI
- Minor fixes and improvements

There is also a news system to build conda binaries. We hope it will not create problems on your system

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)
