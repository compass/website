---
title: "Release of v5.2.0"
date: 2021-07-13
---

COMPASS v5.2.0 release notes:

- Add linear generic controller (cf. [Tutorials](/tutorials/) page for more informations)
- Remove unused `nvalid` argument from controller signatures
- Debug WFS NCPA that were applied twice
- Debug RTC standalone
- Debug P2P GPU access
- Debug roket script in guardians
- Pytests debug
- Debug centroider to make it right with any number of sub-aperture and pixel per subap
- Debug geometric slope computation

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)
