---
title:  "Release of v1.1"
date: 2018-04-01
---

the Easter Bunny came and before hiding chocolate eggs everywhere, it came with a new update of compass V1.1

- update parameter files
- add pyr_misalignments
- add rtc_standalone
- add dm_standalone
- add supervisor between simulation and widget
