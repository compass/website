---
title:  "Release of v4.4.0"
date: 2020-01-15
---

- Debug issue with Kepler architecture
- Multi GPU controller reworked
- Update pages-doc
- Add useful keyworks in```rtc_cacao``` loopframe
- Add ```reset_coms``` function in```sutra_controller```
- Update Jenkinsfile

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)
