---
title:  "Release of v4.1.0"
date: 2019-03-15
---

- Add multiple input/computation/output type for RTC module
- uniformize axis in widget display
- better ELT Pupil generation
- add unit tests of rtc module
- add DM petal
- add fake camera input
- debug ```load_config_from_file```
- debug ```wfs_init```

for Internal developments:

- add compile script

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)