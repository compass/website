---
title:  "Release of v3.4"
date: 2019-01-14
---

- Naga anf Shesha are pur-python module
- CarmaWrap and SheshaWrap are pur-wrapper module using pybind
- minor debug

for Internal developments:

- rename internal variables

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)