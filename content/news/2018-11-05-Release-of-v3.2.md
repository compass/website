---
title:  "Release of v3.2"
date: 2018-11-05
---

- Re-up the database feature that allows to skip initialisation phase by re-using results of previous similar simulation runs

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)