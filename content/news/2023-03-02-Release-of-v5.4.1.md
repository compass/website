---
title: "Release of v5.4.1"
date: 2023-03-16
---

COMPASS v5.4.1 release notes:

- Coronagraph hotfix
- update LICENSE to LGPL

There is also a news system to build conda binaries. We hope it will not create problems on your system

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)
