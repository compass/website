---
title:  "Release of v2.1"
date: 2018-06-26
---

- update documentation

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)