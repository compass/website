---
title:  "Release of v4.0.1"
date: 2019-02-02
---

- minor changes

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)