---
title:  "Release of v3.0"
date: 2018-10-08
---

- Binding based on pyBind11
- N-faces pyramid WFS
- Introduce masked pixels centroider
- Introduce GUARDIANS package
- Introduce a way to check installation
- Debug pupil alignment (for non circular pupil)
- Shesha supervisor module improvement
- Remove the use of bytes (string instead)

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)