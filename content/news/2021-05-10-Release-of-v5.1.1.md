---
title: "Release of v5.1.1"
date: 2021-05-10
---

COMPASS v5.1.1 release notes:

- New class ParamConfig to handle parameters configuration : supervisor constructor requires an instance of it now
- Multi GPU controller generic improvements
- Standalone RTC debug + pytests
- Add leaky factor in the generic controller
- Add [CLOSE](https://arxiv.org/abs/2103.09921) algorithm implementation
- Multi controllers support in the supervisor
- Sub-pixels move for phase screens
- GuARDIANS package updated
- Code documentation update

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)