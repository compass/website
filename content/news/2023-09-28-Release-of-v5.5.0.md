---
title: "Release of v5.5.0"
date: 2023-09-28
---

COMPASS v5.5.0 release notes:

- Add cuda stream in all centroiders
- Replace tqdm with rich
- Change C++ ProgressBar
- Remove cosmic conan repository
- Unzip SAXODmData.zip

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)
