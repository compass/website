---
title: "Release of v5.5.0"
date: 2023-11-08
---

Now COMPASS v5.5.0 can run with CUDA 12, to install it:
```bash
conda install -c "compass/label/cuda12" compass
```

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)
