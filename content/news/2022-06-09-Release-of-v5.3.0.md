---
title: "Release of v5.3.0"
date: 2022-06-09
---

COMPASS v5.3.0 release notes:

- **New feature**: User-defined phase screens circular buffer in the telescope. Allows to put a cube of phase screens as an additional input of the telescope.
- **New feature**: Field stop can now be used with SHWFS. Does not simulate flux reduction.
- Modification of the custom DM construction: see the [new tutorial dedicated to this topic](https://compass.pages.obspm.fr/website/tutorials/2022-06-09-custom-dm)
- Support for clang compiler
- Fix a bug of the KL basis computation due to the move from MAGMA to CUSOLVER library
- Minor fixes

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)
