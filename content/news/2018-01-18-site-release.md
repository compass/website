---
title:  "Site release"
date: 2018-01-18
---

Finally, we did it!! Here is the all the information you need concerning COMPASS, including installation instructions and an user manual.

We will continue to update this site as we are still developing the platform.

All important update on the Git repository will be detailed on the News page: follow it to get informed of each new feature or bug resolution!
