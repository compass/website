---
title:  "Release of v4.0"
date: 2019-01-18
---

- change centroid computation using CUB library

for Internal developments:

- rename internal variables

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)