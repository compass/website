---
title: "Release of v5.4.3"
date: 2023-04-21
---

COMPASS v5.4.3 release notes:

- Add a threshold in the wcog centroider
- Use ```dm_index``` instead of 0 in ```get_influ_function```
- Replace all deprecated np.bool by native bool

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)
