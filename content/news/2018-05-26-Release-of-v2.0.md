---
title:  "Release of v2.0"
date: 2018-05-26
---

- code refactored
- Debug SH spots and PSF moves
- Debug Widget
- Fix build failure with cython 0.28
- Other minor debug

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)