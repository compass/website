---
title: "Release of v6.0 - Full open-source"
date: 2024-10-16
---

We are pleased to announce the release of COMPASS v6.0. With this release, COMPASS becomes
fully open-source and is now available on [Gitlab](https://gitlab.obspm.fr/cosmic-rtc/compass).
The previous Github repository will be archived and will no longer be updated.

The licensing scheme relies on the [LGPLv3](https://www.gnu.org/licenses/lgpl-3.0.html) license.
Please, read the [license](https://gitlab.obspm.fr/cosmic-rtc/compass/-/blob/main/LICENSE) before using COMPASS,
and the [contribution guidelines](https://gitlab.obspm.fr/cosmic-rtc/compass/-/blob/main/CONTRIBUTING.md) if you want to contribute.

This version also comes with changes:
- Add COSMIC RTC interface
- Remove Naga python package : not used, and there is no sense to maintain it as cupy exists
- Remove FP16 support : not used
- Use VCPKG for dependencies instead of Conan
- Code cleaning and renaming of some functions to follow the google naming convention

Thus, this version is not backward compatible with the previous one as it includes breaking changes
in the API. 