---
title:  "Release of v4.3.2"
date: 2019-11-27
---

- Support for controller delay > 2.0
- Multi-GPU controller
- Debug FP16 feature
- Debug 2matrices mode of generic controller
- Minor debug

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)
