---
title:  "Release of v3.3"
date: 2018-11-06
---

- minor changes

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)