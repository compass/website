---
title: "Release of v5.4.2"
date: 2023-04-04
---

COMPASS v5.4.2 release notes:

- Hotfix: remove clipping from pyramid slope-based kernel
- Add python 3.10 support

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)
