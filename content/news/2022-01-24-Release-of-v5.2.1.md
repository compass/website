---
title: "Release of v5.2.1"
date: 2022-01-24
---

COMPASS v5.2.1 release notes:

- Add new geometric WFS method which takes into account pupil masks
- Bug fixes:
    - Wind interpolation
    - Slope-based pyramid centroider "pyr"
    - Pupil transpose
    - Generic linear controller

Report bugs and issues on [this page](https://github.com/ANR-COMPASS/shesha/issues)
