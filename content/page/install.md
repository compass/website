---
title: Installation
layout: article
aside:
  toc: true
---

## Requirements

- Linux distribution with wget and git installed
- Nvidia GPU card with [CUDA toolkit](https://developer.nvidia.com/cuda-downloads) >= 11 (Older versions could be available on request)

## Install Miniforge3 with python3

more info: <https://github.com/mamba-org/mamba>

### setup .bashrc

```bashrc
export MAMBA_ROOT=$HOME/miniforge3
export PATH=$MAMBA_ROOT/bin:$PATH
```

### Download and installation

```bash
wget https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-Linux-x86_64.sh
bash Miniforge3-Linux-x86_64.sh -b -p $MAMBA_ROOT
mamba init
```

## Install COMPASS

### Download source code

First check out the latest version from the GitHub repository :

```bash
git clone https://github.com/COSMIC-RTC/compass.git
```

Once there, you need to modify environment variables in your .bashrc :

```bash
## CUDA default definitions
export CUDA_ROOT=/usr/local/cuda # Be aware that this location may change depending on your CUDA installation
export CUDA_INC_PATH=$CUDA_ROOT/include
export CUDA_LIB_PATH=$CUDA_ROOT/lib
export CUDA_LIB_PATH_64=$CUDA_ROOT/lib64
export PATH=$CUDA_ROOT/bin:$PATH
export LD_LIBRARY_PATH=$CUDA_LIB_PATH_64:$CUDA_LIB_PATH:$LD_LIBRARY_PATH

#COMPASS default definitions
export COMPASS_ROOT=$HOME/compass
export COMPASS_INSTALL_ROOT=$COMPASS_ROOT/local
export SHESHA_ROOT=$COMPASS_ROOT/shesha
export LD_LIBRARY_PATH=$COMPASS_INSTALL_ROOT/lib:$LD_LIBRARY_PATH
export PYTHONPATH=$NAGA_ROOT:$SHESHA_ROOT:$COMPASS_INSTALL_ROOT/python:$PYTHONPATH
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$COMPASS_INSTALL_ROOT/lib/pkgconfig
```

### Install dependencies (if not already done)

```bash
cd $COMPASS_ROOT
mamba env create --file environment.yml
mamba activate compass
export VCPKG_ROOT=$HOME/vcpkg
export PATH=$VCPKG_ROOT:$PATH
./script/install_vcpkg.sh $VCPKG_ROOT
```

### Compilation & Installation

```bash
cd $COMPASS_ROOT
./compile_vcpkg.py
```
You will need to recompile the project each time you change the C++/CUDA code.

## Test your installation

Once the installation is complete, verify that everything is working fine :
```bash
cd $SHESHA_ROOT/tests
./checkCompass.sh
```
This test will basically launch fast simulation test cases and it will print if those cases have been correctly initialised.

*NOTE:* The test_dm_custom tests could failed because of a missing fits file. You can ignore this error. 

## Run the simulation

You are ready !
You can try it with one of our paramaters file:

```bash
cd $SHESHA_ROOT
ipython -i shesha/scripts/closed_loop.py data/par/par4bench/scao_sh_16x16_8pix.py
```

And if you want to launch the GUI:

```bash
cd $SHESHA_ROOT
ipython -i shesha/widgets/widget_ao.py
```

Browse through the [manual](page/manual/) to understand how to use COMPASS.
